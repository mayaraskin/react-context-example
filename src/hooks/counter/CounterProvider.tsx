import React, {createContext, useState, ReactNode } from "react";

export type ProviderValue = {
    state: { counter: number },
    actions: { setCounter: (value: number) => void }
}

export const CounterContext = createContext({})

type ProviderProps = {
    children: ReactNode | ReactNode[]
}

const CounterProvider = ({ children }: ProviderProps) => {
    const [ counter, setCounter ] = useState<number>(0)
    const value: ProviderValue = { state: { counter }, actions: { setCounter } };

    return <CounterContext.Provider value={value}>
        { children }
    </CounterContext.Provider>
}

export default CounterProvider