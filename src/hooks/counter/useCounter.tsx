import {CounterContext, ProviderValue} from "./CounterProvider";
import {useContext} from "react";

const useCounter = () => {
    const { state: { counter }, actions: { setCounter } } : ProviderValue = useContext(CounterContext)
    return { counter: counter, setCounter: setCounter }
}

export default useCounter