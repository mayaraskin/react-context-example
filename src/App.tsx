import './App.css'
import Counter from "./components/Counter";
import CounterProvider from "./hooks/counter/CounterProvider";
import ConsumerTest from "./components/ConsumerTest";

function App() {
    return (
        <CounterProvider>
            <Counter/>
            <ConsumerTest/>
        </CounterProvider>
    )
}

export default App
