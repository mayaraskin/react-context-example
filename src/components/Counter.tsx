import useCounter from "../hooks/counter/useCounter";


const Counter = () => {
    const { counter, setCounter } = useCounter()

    return (
        <div className="card">
            <button onClick={() => setCounter(counter + 1)}>
                <h5 style={{ color: 'red', margin: 0 }}>count is {counter}</h5 >
            </button>
        </div>
    )
}

export default Counter