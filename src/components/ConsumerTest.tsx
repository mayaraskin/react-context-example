import useCounter from "../hooks/counter/useCounter";

// This component re-renders on counter updates as well
const ConsumerTest = () => {
    const { setCounter } = useCounter()

    return <>
       <button style={{ color: 'blue' }} onClick={() => setCounter(Math.random())}>Test</button>
    </>
}

export default ConsumerTest